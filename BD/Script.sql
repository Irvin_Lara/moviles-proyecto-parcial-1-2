-- Drop Tables --

DROP TABLE IF EXISTS alumnos_grupos;

DROP TABLE IF EXISTS alumnos;

DROP TABLE IF EXISTS grupos;

-- Create Tables --

CREATE TABLE alumnos(id_alumno INTEGER PRIMARY KEY AUTOINCREMENT,
                    nombre_alumno TEXT NOT NULL,
                    apellido_paterno TEXT NOT NULL,
                    apellido_materno TEXT NOT NULL);

CREATE TABLE grupos(id_grupo INTEGER PRIMARY KEY AUTOINCREMENT,
                    nombre_grupo TEXT NOT NULL,
                    asistencias_totales INTEGER NOT NULL,
                    UNIQUE(nombre_grupo));

CREATE TABLE alumnos_grupos(id_alumno INTEGER NOT NULL,
                    id_grupo INTEGER NOT NULL,
                    asistencias INTEGER NOT NULL,
                    PRIMARY KEY (id_alumno, id_grupo),
                    FOREIGN KEY (id_alumno) REFERENCES alumnos (id_alumno)
                    ON DELETE CASCADE ON UPDATE NO ACTION,
                    FOREIGN KEY (id_grupo) REFERENCES grupos (id_grupo)
                    ON DELETE CASCADE ON UPDATE NO ACTION);
