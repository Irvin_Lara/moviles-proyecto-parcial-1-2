package com.example.asistencias;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asistencias.bd.DbHelper;
import com.example.asistencias.modelos.Alumno;
import com.example.asistencias.modelos.AlumnoGrupo;
import com.example.asistencias.modelos.Grupo;

import java.util.ArrayList;

public class ReporteActivity extends AppCompatActivity {

    private TextView textViewReporteAlumnos;
    private TextView textViewAsistenciasMinimas;
    private ListView listViewReporteAlumnos;
    private ArrayList<Alumno> alumnos;
    private ArrayAdapter<Alumno> adapterAlumnos;
    private DbHelper dbHelper;
    private Grupo grupo;
    private int cont = 0;
    private int cont2 = 0;

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporte);

        Intent intent = getIntent();

        setGrupo((Grupo) intent.getParcelableExtra("grupo"));

        initializeActivity();
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        if(cont > 0) {
            alumnos.clear();
            alumnos.addAll(dbHelper.consultarAlumnosDeUnGrupo(getGrupo()));
            adapterAlumnos.notifyDataSetChanged();
        }
        cont ++;
    }

    private void initializeActivity() {
        textViewReporteAlumnos = findViewById(R.id.textViewReporteAlumnos);
        textViewAsistenciasMinimas = findViewById(R.id.textViewAsistenciasMinimas);
        listViewReporteAlumnos = findViewById(R.id.listViewReporteAlumnos);

        textViewReporteAlumnos.setText("Reporte de Alumnos del grupo: " + getGrupo().getNombreGrupo());
        textViewAsistenciasMinimas.setText("Las asistencias minimas para pasar fueron: " + getGrupo().getAsistenciasTotales());

        dbHelper = DbHelper.returnBD(getBaseContext());

        alumnos = dbHelper.consultarAlumnosDeUnGrupo(getGrupo());

        adapterAlumnos =  new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                alumnos
        );

        listViewReporteAlumnos.setAdapter(adapterAlumnos);


        /*
        cont2 = 0;
        for(Alumno alumno : alumnos) {
            AlumnoGrupo alumnoGrupo = new AlumnoGrupo(alumno, getGrupo());
            int asistenciasAlumno = dbHelper.consultarAsistencias(alumnoGrupo);
            if (asistenciasAlumno < getGrupo().getAsistenciasTotales()) {
            //    listViewReporteAlumnos.getChildAt(cont2).setBackgroundColor(Color.rgb(244, 83, 66));
            } else {
             //   listViewReporteAlumnos.getChildAt(cont2).setBackgroundColor(Color.rgb(140, 244, 66));
            }
            cont2++;
        }
        */

        listViewReporteAlumnos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Alumno alumno = ((Alumno) listViewReporteAlumnos.getItemAtPosition(position));
                AlumnoGrupo alumnoGrupo = new AlumnoGrupo(alumno, getGrupo());
                int asistencias = dbHelper.consultarAsistencias(alumnoGrupo);

                if (asistencias < getGrupo().getAsistenciasTotales()) {
                    listViewReporteAlumnos.getChildAt(position).setBackgroundColor(Color.rgb(244, 83, 66));
                    Toast.makeText(getBaseContext(), "Reprobado por tener " + asistencias + " asistencias", Toast.LENGTH_LONG).show();
                } else {
                    listViewReporteAlumnos.getChildAt(position).setBackgroundColor(Color.rgb(140, 244, 66));
                    Toast.makeText(getBaseContext(), "Pasa por tener " + asistencias + " asistencias", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
