package com.example.asistencias.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.asistencias.modelos.Alumno;
import com.example.asistencias.modelos.AlumnoGrupo;
import com.example.asistencias.modelos.Grupo;

import java.util.ArrayList;
import java.util.List;

public class DbHelper extends SQLiteOpenHelper {

    public static DbHelper hipotecaDbHelper = null;
    private static int version = 1;
    private static String name = "AsistenciasBD" ;
    private static CursorFactory factory = null;
    private SQLiteDatabase db;

    private DbHelper(Context context) {
        super(context, name, factory, version);
        db=this.getWritableDatabase();
        System.out.println("Se creó la bd por primera vez");
    }

    public static synchronized DbHelper returnBD(Context context) {
        if (hipotecaDbHelper == null) {
            hipotecaDbHelper = new DbHelper(context);
        }
        return hipotecaDbHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL( "CREATE TABLE alumnos(" +
                " id_alumno INTEGER PRIMARY KEY AUTOINCREMENT, " +
                " nombre_alumno TEXT NOT NULL, " +
                " apellido_paterno TEXT NOT NULL, " +
                " apellido_materno TEXT NOT NULL)" );

        db.execSQL( "CREATE TABLE grupos(" +
                " id_grupo INTEGER PRIMARY KEY AUTOINCREMENT, " +
                " nombre_grupo TEXT NOT NULL, " +
                " asistencias_totales INTEGER NOT NULL, " +
                " UNIQUE(nombre_grupo))" );

        db.execSQL( "CREATE TABLE alumnos_grupos(" +
                " id_alumno INTEGER NOT NULL, " +
                " id_grupo INTEGER NOT NULL, " +
                " asistencias INTEGER NOT NULL, " +
                " PRIMARY KEY (id_alumno, id_grupo), " +
                " FOREIGN KEY (id_alumno) REFERENCES alumnos (id_alumno) ON DELETE CASCADE ON UPDATE NO ACTION, " +
                " FOREIGN KEY (id_grupo) REFERENCES grupos (id_grupo) ON DELETE CASCADE ON UPDATE NO ACTION)" );

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void reiniciarBase() {
        db.execSQL("DROP TABLE alumnos_grupos");
        db.execSQL("DROP TABLE alumnos");
        db.execSQL("DROP TABLE grupos");

        db.execSQL( "CREATE TABLE alumnos(" +
                " id_alumno INTEGER PRIMARY KEY AUTOINCREMENT, " +
                " nombre_alumno TEXT NOT NULL, " +
                " apellido_paterno TEXT NOT NULL, " +
                " apellido_materno TEXT NOT NULL)" );

        db.execSQL( "CREATE TABLE grupos(" +
                " id_grupo INTEGER PRIMARY KEY AUTOINCREMENT, " +
                " nombre_grupo TEXT NOT NULL, " +
                " asistencias_totales INTEGER NOT NULL, " +
                " UNIQUE(nombre_grupo))" );

        db.execSQL( "CREATE TABLE alumnos_grupos(" +
                " id_alumno INTEGER NOT NULL, " +
                " id_grupo INTEGER NOT NULL, " +
                " asistencias INTEGER NOT NULL, " +
                " PRIMARY KEY (id_alumno, id_grupo), " +
                " FOREIGN KEY (id_alumno) REFERENCES alumnos (id_alumno) ON DELETE CASCADE ON UPDATE NO ACTION, " +
                " FOREIGN KEY (id_grupo) REFERENCES grupos (id_grupo) ON DELETE CASCADE ON UPDATE NO ACTION)" );
    }

    public void insertarAlumno(Alumno alumno){
        ContentValues cv = new ContentValues();
        cv.put("nombre_alumno", alumno.getNombreAlumno());
        cv.put("apellido_paterno", alumno.getApellidoPaterno());
        cv.put("apellido_materno", alumno.getApellidoMaterno());
        db.insert("alumnos", null, cv);
        System.out.println("Se insertó correctamente el alumno");
    }

    public void insertarGrupo(Grupo grupo){
        ContentValues cv = new ContentValues();
        cv.put("nombre_grupo", grupo.getNombreGrupo());
        cv.put("asistencias_totales", grupo.getAsistenciasTotales());
        db.insert("grupos", null, cv);
        System.out.println("Se insertó correctamente el grupo");
    }

    public void insertarAlumnoGrupo(AlumnoGrupo alumnoGrupo){
        ContentValues cv = new ContentValues();
        cv.put("id_alumno", alumnoGrupo.getAlumno().getIdAlumno());
        cv.put("id_grupo", alumnoGrupo.getGrupo().getIdGrupo());
        cv.put("asistencias", alumnoGrupo.getAsistencias());
        db.insert("alumnos_grupos", null, cv);
        System.out.println("Se insertó el alumno-grupo: " + alumnoGrupo.getAlumno().getIdAlumno() + " " + alumnoGrupo.getGrupo().getIdGrupo() + " " + alumnoGrupo.getAsistencias());
        System.out.println("Se insertó correctamente el alumno-grupo");
    }

    public void insertarAsistencia(AlumnoGrupo alumnoGrupo) {
        int asistenciasAnteriores = 0;
        Cursor c = db.rawQuery("SELECT * FROM alumnos_grupos WHERE id_alumno = ? AND id_grupo = ?", new String[] {String.valueOf(alumnoGrupo.getAlumno().getIdAlumno()), String.valueOf(alumnoGrupo.getGrupo().getIdGrupo())});

        if (c.moveToFirst()) {
            do {
                asistenciasAnteriores = c.getInt(2);
            } while(c.moveToNext());
        }

        int asistenciasNuevas = asistenciasAnteriores + 1;

        ContentValues cv = new ContentValues();
        cv.put("asistencias", asistenciasNuevas);
        db.update("alumnos_grupos", cv, "id_alumno = ? AND id_grupo = ?", new String[] {String.valueOf(alumnoGrupo.getAlumno().getIdAlumno()), String.valueOf(alumnoGrupo.getGrupo().getIdGrupo())});

    }

    public int consultarAsistencias(AlumnoGrupo alumnoGrupo) {
        int asistenciasAnteriores = 0;
        Cursor c = db.rawQuery("SELECT * FROM alumnos_grupos WHERE id_alumno = ? AND id_grupo = ?", new String[] {String.valueOf(alumnoGrupo.getAlumno().getIdAlumno()), String.valueOf(alumnoGrupo.getGrupo().getIdGrupo())});

        if (c.moveToFirst()) {
            do {
                asistenciasAnteriores = c.getInt(2);
            } while(c.moveToNext());
        }
        return asistenciasAnteriores;
    }

    public ArrayList<Alumno> consultarAlumnos() {
        ArrayList<Alumno> alumnos = new ArrayList<>();
        Cursor c = db.rawQuery("SELECT * FROM alumnos", null);

        if (c.moveToFirst()) {
            do {
                int idAlumno = c.getInt(0);
                String nombreAlumno = c.getString(1);
                String apellido_paterno = c.getString(2);
                String apellido_materno = c.getString(3);
                System.out.println("El alumno es: " + nombreAlumno);
                Alumno alumno = new Alumno(idAlumno, nombreAlumno, apellido_paterno, apellido_materno);
                alumnos.add(alumno);
            } while(c.moveToNext());
        }
        System.out.println("Se listaron todos los alumnos");
        return alumnos;
    }

    public ArrayList<Alumno> consultarAlumnosDeUnGrupo(Grupo grupo) {
        ArrayList<Alumno> alumnos = new ArrayList<>();

        Cursor c = db.rawQuery("SELECT * FROM alumnos a INNER JOIN alumnos_grupos g ON a.id_alumno = g.id_alumno WHERE g.id_grupo = ?", new String[] {String.valueOf(grupo.getIdGrupo())});

        if (c.moveToFirst()) {
            do {
                int idAlumno = c.getInt(0);
                String nombreAlumno = c.getString(1);
                String apellido_paterno = c.getString(2);
                String apellido_materno = c.getString(3);

                Alumno alumno = new Alumno(idAlumno, nombreAlumno, apellido_paterno, apellido_materno);
                alumnos.add(alumno);
            } while(c.moveToNext());
        }

        System.out.println("Se listaron los alumnos que están en este grupo");
        for (Alumno alumno : alumnos) {
            System.out.println("El alumno es: " + alumno);
        }
        return alumnos;
    }

    public ArrayList<Alumno> consultarAlumnosNoDeUnGrupo(Grupo grupo) {
        ArrayList<Alumno> alumnos = new ArrayList<>();
        List<Integer> idAlumnos = new ArrayList<>();
        List<Integer> idAlumnos2 = new ArrayList<>();

        Cursor c = db.rawQuery("SELECT id_alumno FROM alumnos" , null);

        if (c.moveToFirst()) {
            do {
                idAlumnos.add(c.getInt(0));
            } while(c.moveToNext());
        }

        Cursor c2 = db.rawQuery("SELECT id_alumno FROM alumnos_grupos WHERE id_grupo = ?" , new String[] {String.valueOf(grupo.getIdGrupo())});

        if (c2.moveToFirst()) {
            do {
                idAlumnos2.add(c2.getInt(0));
            } while(c2.moveToNext());
        }

        System.out.println("Id2");
        for(int idAlumno : idAlumnos2) {

            System.out.println(idAlumno);
        }

        System.out.println("Antes");
        for(int idAlumno : idAlumnos) {

            System.out.println(idAlumno);
        }

        idAlumnos.removeAll(idAlumnos2);

        System.out.println("Despues");
        for(int idAlumno : idAlumnos) {

            System.out.println(idAlumno);
        }

        for (int idAlumno : idAlumnos) {
            Cursor c3 = db.rawQuery("SELECT * FROM alumnos WHERE id_alumno = ?" , new String[] {String.valueOf(idAlumno)});

            if (c3.moveToFirst()) {
                do {
                    String nombreAlumno = c3.getString(1);
                    String apellido_paterno = c3.getString(2);
                    String apellido_materno = c3.getString(3);

                    Alumno alumno = new Alumno(idAlumno, nombreAlumno, apellido_paterno, apellido_materno);
                    alumnos.add(alumno);
                } while(c3.moveToNext());
            }
        }

        System.out.println("Se listaron los alumnos que no están en este grupo");
        return alumnos;
    }

    public ArrayList<Grupo> consultarGrupos() {
        ArrayList<Grupo> grupos = new ArrayList<>();
        Cursor c = db.rawQuery("SELECT * FROM grupos", null);

        if (c.moveToFirst()) {
            do {
                int idGrupo = c.getInt(0);
                String nombreGrupo = c.getString(1);
                int asistenciasTotales = c.getInt(2);
                Grupo grupo = new Grupo(idGrupo, nombreGrupo, asistenciasTotales);
                grupos.add(grupo);
            } while(c.moveToNext());
        }
        System.out.println("Se listaron los grupos correctamente");
        return grupos;
    }

    public void eliminarAlumno(Alumno alumno) {
        db.delete("alumnos_grupos", "id_alumno = " + alumno.getIdAlumno(), null);
        db.delete("alumnos", "id_alumno = " + alumno.getIdAlumno(), null);
    }

    public void eliminarGrupo(Grupo grupo) {
        db.delete("alumnos_grupos", "id_grupo = " + grupo.getIdGrupo(), null);
        db.delete("grupos", "id_grupo = " + grupo.getIdGrupo(), null);
    }

    public void eliminarAlumnoDeGrupo(Alumno alumno, Grupo grupo) {
        db.delete("alumnos_grupos", "id_alumno = " + alumno.getIdAlumno() + " AND id_grupo = " + grupo.getIdGrupo(), null);
    }

}