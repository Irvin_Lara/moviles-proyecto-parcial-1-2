package com.example.asistencias;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.asistencias.bd.DbHelper;
import com.example.asistencias.modelos.Grupo;

public class GruposActivity extends AppCompatActivity {

    private EditText editTextNombreGrupo;
    private EditText editTextAsistenciasTotales;
    private Button buttonAltaGrupo;
    private DbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grupos);

        initializeActivity();
    }

    private void initializeActivity() {
        buttonAltaGrupo = findViewById(R.id.buttonAltaAlumno);
        editTextNombreGrupo = findViewById(R.id.editTextNombreGrupo);
        editTextAsistenciasTotales = findViewById(R.id.editTextAsistenciasTotales);

        dbHelper = DbHelper.returnBD(getBaseContext());

        buttonAltaGrupo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextNombreGrupo.getText().toString().equals("") || editTextAsistenciasTotales.getText().toString().equals("")) {
                    Toast.makeText(getBaseContext(), "No puedes dejar campos vacíos", Toast.LENGTH_LONG).show();
                } else {
                    String nombreGrupo = editTextNombreGrupo.getText().toString();
                    int limiteFaltas = Integer.parseInt(editTextAsistenciasTotales.getText().toString());

                    Grupo grupo = new Grupo(0, nombreGrupo, limiteFaltas);

                    dbHelper.insertarGrupo(grupo);

                    Toast.makeText(getBaseContext(), "Ha agregado al grupo " + grupo.getNombreGrupo() + " correctamente", Toast.LENGTH_LONG).show();
                    finish();
                }
            }

        });
    }
}
