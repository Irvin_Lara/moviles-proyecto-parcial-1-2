package com.example.asistencias;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asistencias.bd.DbHelper;
import com.example.asistencias.modelos.Alumno;
import com.example.asistencias.modelos.AlumnoGrupo;
import com.example.asistencias.modelos.Grupo;

import java.util.ArrayList;

public class AsistenciaActivity extends AppCompatActivity {

    private Button buttonAltaAlumnoGrupo;
    private Button buttonReporte;
    private TextView textViewAlumnosDelGrupo;
    private ListView listViewAsistencias;
    private ArrayList<Alumno> alumnos;
    private ArrayAdapter<Alumno> adapterAlumnos;
    private DbHelper dbHelper;
    private Grupo grupo;
    private int cont = 0;

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asistencia);

        Intent intent = getIntent();

        setGrupo((Grupo) intent.getParcelableExtra("grupo"));

        initializeActivity();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.menu_asistencias, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Alumno alumno;
        AlumnoGrupo alumnoGrupo;
        switch (item.getItemId()) {
            case R.id.itemAsistencia:
                alumno = alumnos.get(info.position);
                alumnoGrupo = new AlumnoGrupo(alumno, getGrupo());
                dbHelper.insertarAsistencia(alumnoGrupo);
                listViewAsistencias.getChildAt(info.position).setBackgroundColor(Color.rgb(140, 244, 66));
                Toast.makeText(getBaseContext(), "Asistencia para: " + alumno.getNombreAlumno(), Toast.LENGTH_LONG).show();
                return true;
            case R.id.itemBorrar:
                alumno = alumnos.get(info.position);
                dbHelper.eliminarAlumnoDeGrupo(alumno, getGrupo());
                alumnos.remove(info.position);
                adapterAlumnos.notifyDataSetChanged();
                Toast.makeText(getBaseContext(), "Ha borrado a: " + alumno.getNombreAlumno() + " del grupo", Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        if(cont > 0) {
            alumnos.clear();
            alumnos.addAll(dbHelper.consultarAlumnosDeUnGrupo(getGrupo()));
            adapterAlumnos.notifyDataSetChanged();
        }
        cont ++;
    }

    private void initializeActivity() {
        buttonAltaAlumnoGrupo = findViewById(R.id.buttonAltaAlumnoGrupo);
        buttonReporte = findViewById(R.id.buttonReporte);
        textViewAlumnosDelGrupo = findViewById(R.id.textViewAlumGru);
        listViewAsistencias = findViewById(R.id.listViewAsistencias);

        textViewAlumnosDelGrupo.setText("Alumnos del grupo: " + getGrupo().getNombreGrupo() + " (Asistencias minimas: " +getGrupo().getAsistenciasTotales() + ")");

        registerForContextMenu(listViewAsistencias);

        dbHelper = DbHelper.returnBD(getBaseContext());

        alumnos = dbHelper.consultarAlumnosDeUnGrupo(getGrupo());

        adapterAlumnos =  new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                alumnos
        );

        listViewAsistencias.setAdapter(adapterAlumnos);

        buttonAltaAlumnoGrupo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AsistenciaActivity.this, AlumnoGrupoActivity.class);
                intent.putExtra("grupo", getGrupo());
                startActivity(intent);
            }

        });

        buttonReporte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AsistenciaActivity.this, ReporteActivity.class);

                intent.putExtra("grupo", getGrupo());

                startActivity(intent);
            }

        });

        listViewAsistencias.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Alumno alumno = ((Alumno) listViewAsistencias.getItemAtPosition(position));
                AlumnoGrupo alumnoGrupo = new AlumnoGrupo(alumno, getGrupo());
                int asistencias = dbHelper.consultarAsistencias(alumnoGrupo);
                Toast.makeText(getBaseContext(), alumno.getNombreAlumno() + " Tiene " + asistencias + " asistencias", Toast.LENGTH_LONG).show();
            }
        });

    }
}
