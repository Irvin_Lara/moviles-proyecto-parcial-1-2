package com.example.asistencias.modelos;

public class Alumno {

    private int idAlumno;
    private String nombreAlumno;
    private String apellidoPaterno;
    private String apellidoMaterno;

    public Alumno() {

    }

    public Alumno(int idAlumno) {
        this.idAlumno = idAlumno;
    }

    public Alumno(int idAlumno, String nombreAlumno, String apellidoPaterno, String apellidoMaterno) {
        this.idAlumno = idAlumno;
        this.nombreAlumno = nombreAlumno;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
    }

    public int getIdAlumno() {
        return idAlumno;
    }

    public void setIdAlumno(int idAlumno) {
        this.idAlumno = idAlumno;
    }

    public String getNombreAlumno() {
        return nombreAlumno;
    }

    public void setNombreAlumno(String nombreAlumno) {
        this.nombreAlumno = nombreAlumno;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    @Override
    public String toString() {
        return getApellidoPaterno() + " " + getApellidoMaterno() + " " + getNombreAlumno();
    }

}
