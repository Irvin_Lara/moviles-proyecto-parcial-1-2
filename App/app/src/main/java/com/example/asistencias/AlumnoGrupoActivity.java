package com.example.asistencias;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asistencias.bd.DbHelper;
import com.example.asistencias.modelos.Alumno;
import com.example.asistencias.modelos.AlumnoGrupo;
import com.example.asistencias.modelos.Grupo;

import java.util.ArrayList;

public class AlumnoGrupoActivity extends AppCompatActivity {

    private TextView textViewAlumnosDelGrupo;
    private ListView listViewAlumnosParaAlta;
    private ArrayList<Alumno> alumnos;
    private ArrayAdapter<Alumno> adapterAlumnos;
    private DbHelper dbHelper;
    private Grupo grupo;
    private Alumno alumno;
    private int position;
    private int cont = 0;
    private int cont2 = 0;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alumno_grupo);

        Intent intent = getIntent();

        setGrupo((Grupo) intent.getParcelableExtra("grupo"));

        initializeActivity();
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        if(cont2 > 0) {
            alumnos.clear();
            alumnos.addAll(dbHelper.consultarAlumnosNoDeUnGrupo(getGrupo()));
            adapterAlumnos.notifyDataSetChanged();
        }
        cont2 ++;
    }

    private void initializeActivity() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Agregará al alumno");
        builder.setMessage("¿Está seguro?");

        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                AlumnoGrupo alumnoGrupo = new AlumnoGrupo(getAlumno(), getGrupo(), 0);
                dbHelper.insertarAlumnoGrupo(alumnoGrupo);
                alumnos.remove(getPosition());
                adapterAlumnos.notifyDataSetChanged();
                Toast.makeText(getBaseContext(), "Se agregó correctamente el alumno al grupo", Toast.LENGTH_LONG).show();
                cont = 0;
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        textViewAlumnosDelGrupo = findViewById(R.id.textViewAlumnGrup);
        listViewAlumnosParaAlta = findViewById(R.id.listViewAlumnosParaAlta);

        textViewAlumnosDelGrupo.setText("Buscar alumnos disponibles para el grupo: " + getGrupo().getNombreGrupo());

        dbHelper = DbHelper.returnBD(getBaseContext());

        alumnos = dbHelper.consultarAlumnosNoDeUnGrupo(getGrupo());

        adapterAlumnos =  new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                alumnos
        );

        listViewAlumnosParaAlta.setAdapter(adapterAlumnos);

        listViewAlumnosParaAlta.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setAlumno((Alumno) listViewAlumnosParaAlta.getItemAtPosition(position));
                setPosition(position);
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        
    }
}
