package com.example.asistencias.modelos;

import android.os.Parcel;
import android.os.Parcelable;

public class Grupo implements Parcelable {

    private int idGrupo;
    private String nombreGrupo;
    private int asistenciasTotales;

    public Grupo() {

    }

    public Grupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    public Grupo(int idGrupo, String nombreGrupo, int asistenciasTotales) {
        this.idGrupo = idGrupo;
        this.nombreGrupo = nombreGrupo;
        this.asistenciasTotales = asistenciasTotales;
    }

    protected Grupo(Parcel in) {
        idGrupo = in.readInt();
        nombreGrupo = in.readString();
        asistenciasTotales = in.readInt();
    }

    public static final Creator<Grupo> CREATOR = new Creator<Grupo>() {
        @Override
        public Grupo createFromParcel(Parcel in) {
            return new Grupo(in);
        }

        @Override
        public Grupo[] newArray(int size) {
            return new Grupo[size];
        }
    };

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }

    public String getNombreGrupo() {
        return nombreGrupo;
    }

    public void setNombreGrupo(String nombreGrupo) {
        this.nombreGrupo = nombreGrupo;
    }

    public int getAsistenciasTotales() {
        return asistenciasTotales;
    }

    public void setAsistenciasTotales(int asistenciasTotales) {
        this.asistenciasTotales = asistenciasTotales;
    }

    @Override
    public String toString() {
        return getNombreGrupo();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idGrupo);
        dest.writeString(nombreGrupo);
        dest.writeInt(asistenciasTotales);
    }
}
