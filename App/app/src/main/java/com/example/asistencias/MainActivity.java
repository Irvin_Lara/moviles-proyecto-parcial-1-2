package com.example.asistencias;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.asistencias.bd.DbHelper;
import com.example.asistencias.modelos.Grupo;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Button buttonAlumnos;
    private Button buttonNuevoGrupo;
    private ListView listViewGrupos;
    private ArrayList<Grupo> grupos;
    private ArrayAdapter<Grupo> adapterGrupos;
    private DbHelper dbHelper;
    private Grupo grupo;
    private int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    private int cont = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeActivity();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.menu_grupos, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.itemBorrar:
                Grupo grupo = grupos.get(info.position);
                dbHelper.eliminarGrupo(grupo);
                grupos.remove(info.position);
                adapterGrupos.notifyDataSetChanged();
                Toast.makeText(getBaseContext(), "Ha borrado a: " + grupo.getNombreGrupo(), Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        if(cont > 0) {
            grupos.clear();
            grupos.addAll(dbHelper.consultarGrupos());
            adapterGrupos.notifyDataSetChanged();
        }
        cont ++;
    }

    private void initializeActivity() {
        buttonAlumnos = findViewById(R.id.buttonAlumnos);
        buttonNuevoGrupo = findViewById(R.id.buttonNuevoGrupo);
        listViewGrupos = findViewById(R.id.listViewGrupos);

        registerForContextMenu(listViewGrupos);

        dbHelper = DbHelper.returnBD(getBaseContext());

        grupos = dbHelper.consultarGrupos();
        //dbHelper.reiniciarBase();

        adapterGrupos =  new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                grupos
        );

        listViewGrupos.setAdapter(adapterGrupos);

        listViewGrupos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setGrupo((Grupo) listViewGrupos.getItemAtPosition(position));
                setPosition(position);
                Intent intent = new Intent(MainActivity.this, AsistenciaActivity.class);
                intent.putExtra("grupo", getGrupo());
                startActivity(intent);
            }
        });

        buttonAlumnos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AlumnosActivity.class);
                startActivity(intent);
            }

        });

        buttonNuevoGrupo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, GruposActivity.class);
                startActivity(intent);
            }

        });
    }
}
