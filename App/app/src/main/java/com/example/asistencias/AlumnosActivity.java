package com.example.asistencias;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.asistencias.bd.DbHelper;
import com.example.asistencias.modelos.Alumno;

import java.util.ArrayList;

public class AlumnosActivity extends AppCompatActivity {

    private Button buttonNuevoAlumno;
    private ListView listViewAlumnos;
    private ArrayList<Alumno> alumnos;
    private ArrayAdapter<Alumno> adapterAlumnos;
    private DbHelper dbHelper;
    private int cont = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alumnos);

        initializeActivity();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.menu_alumnos, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.itemBorrar:
                Alumno alumno = alumnos.get(info.position);
                dbHelper.eliminarAlumno(alumno);
                alumnos.remove(info.position);
                adapterAlumnos.notifyDataSetChanged();
                Toast.makeText(getBaseContext(), "Ha borrado a: " + alumno.getNombreAlumno(), Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        if(cont > 0) {
            alumnos.clear();
            alumnos.addAll(dbHelper.consultarAlumnos());
            adapterAlumnos.notifyDataSetChanged();
        }
        cont ++;
    }

    private void initializeActivity() {
        buttonNuevoAlumno = findViewById(R.id.buttonNuevoAlumno);
        listViewAlumnos = findViewById(R.id.listViewAlumnos);

        registerForContextMenu(listViewAlumnos);

        dbHelper = DbHelper.returnBD(getBaseContext());

        alumnos = dbHelper.consultarAlumnos();

        adapterAlumnos =  new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                alumnos
        );

        listViewAlumnos.setAdapter(adapterAlumnos);

        buttonNuevoAlumno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AlumnosActivity.this, NuevoAlumnoActivity.class);
                startActivity(intent);
            }

        });


    }
}
