package com.example.asistencias.modelos;

public class AlumnoGrupo {

    private Alumno alumno;
    private Grupo grupo;
    private int asistencias;

    public AlumnoGrupo() {

    }

    public AlumnoGrupo(Alumno alumno, Grupo grupo) {
        this.alumno = alumno;
        this.grupo = grupo;
    }

    public AlumnoGrupo(Alumno alumno, Grupo grupo, int asistencias) {
        this.alumno = alumno;
        this.grupo = grupo;
        this.asistencias = asistencias;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public int getAsistencias() {
        return asistencias;
    }

    public void setAsistencias(int asistencias) {
        this.asistencias = asistencias;
    }

}
