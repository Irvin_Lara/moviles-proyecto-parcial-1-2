package com.example.asistencias;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.asistencias.bd.DbHelper;
import com.example.asistencias.modelos.Alumno;

public class NuevoAlumnoActivity extends AppCompatActivity {

    private EditText editTextNombreAlumno;
    private EditText editTextApellidoPaterno;
    private EditText editTextApellidoMaterno;
    private Button buttonAltaAlumno;
    private DbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_alumno);

        initializeActivity();
    }

    private void initializeActivity() {
        buttonAltaAlumno = findViewById(R.id.buttonAltaAlumno);
        editTextNombreAlumno = findViewById(R.id.editTextNombreAlumno);
        editTextApellidoPaterno = findViewById(R.id.editTextApellidoPaterno);
        editTextApellidoMaterno = findViewById(R.id.editTextApellidoMaterno);

        dbHelper = DbHelper.returnBD(getBaseContext());

        buttonAltaAlumno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextNombreAlumno.getText().toString().equals("") || editTextApellidoPaterno.getText().toString().equals("") || editTextApellidoMaterno.getText().toString().equals("")) {
                    Toast.makeText(getBaseContext(), "No puedes dejar campos vacíos", Toast.LENGTH_LONG).show();
                } else {
                    String nombreAlumno = editTextNombreAlumno.getText().toString();
                    String apellidoPaterno = editTextApellidoPaterno.getText().toString();
                    String apellidoMaterno = editTextApellidoMaterno.getText().toString();

                    Alumno alumno = new Alumno(0, nombreAlumno, apellidoPaterno, apellidoMaterno);

                    dbHelper.insertarAlumno(alumno);

                    Toast.makeText(getBaseContext(), "Ha agregado al alumno " + alumno.getNombreAlumno() + " correctamente", Toast.LENGTH_LONG).show();
                    finish();
                }
            }

        });
    }
}
